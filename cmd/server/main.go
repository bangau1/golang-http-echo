package main

import (
	"net/http"
)

func main() {
	go func() {
		println("Setup the liveness/readiness check.")
		http.ListenAndServe(":8081", &healthCheckHandler{})
	}()

	println("Setup the main check")
	//the last call is outside goroutine to avoid that program just exit
	http.ListenAndServe(":8080", &fooHandler{})
}

type fooHandler struct {
}

func (m *fooHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Listening on 8080: foo "))
}

type healthCheckHandler struct {
}

func (m *healthCheckHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Listening on 8081: bar "))
}
